# Crossfade resistor calculator

Here is a pretty nice circuit I found that crossfades between 2 input signals.

![crossfade circuit](crossfade-circuit.jpg)

I didn't know what resistor values to use for it to get a "constant power"
crossfade, so I wrote this python script to brute force the answer by
calculating the crossfade curve for every combination of E12 series resistor
values, comparing them to a sine curve and picking the closest ones.

![test circuit](circuit.png)

The script calculates the values for `R1` and `R2` based on the above circuit.

## Results

If you don't want to run the script yourself, here are the top 11 resistor
pairs. You can swap the values of `R1` and `R2` around if you want but I'd
recommend using the larger resitor for `R1` to get a higher input impedance.

| R1  | R2  | difference         |
|-----|-----|--------------------|
| 68k | 10k | 3.3512400657827426 |
| 33k | 12k | 3.3520417424192086 |
| 56k | 10k | 3.3549836866132505 |
| 82k | 10k | 3.3558699786485384 |
| 22k | 15k | 3.356074339555253  |
| 18k | 18k | 3.360120195247075  |
| 27k | 12k | 3.365163799096108  |
| 47k | 10k | 3.369852848357349  |
| 39k | 12k | 3.3727193303844416 |
| 18k | 15k | 3.376562644048259  |
| 39k | 10k | 3.405273723122694  |

And here are the results of those resistor values plotted out. The blue one
that's a bit different than the others is the reference sine.

![figure](figure.png)
