import matplotlib.pyplot as plt
from math import sin
from math import cos
from math import pi

Rf = 100000
RV = 10000
Vin = 5

E12 = [1, 1.2, 1.5, 1.8, 2.2, 2.7, 3.3, 3.9, 4.7, 5.6, 6.8, 8.2]
available_resistors = [ 1000*x for x in E12 ] + [ 10000*x for x in E12 ]

# Calculate and plot a sine to use as reference
sin_plot = [sin( x/( 400 / (2 * pi) ) ) for x in range(0,101)]
plt.plot(sin_plot)

# Plot voltages of Vout for given values of R1 and R2 over all positions of RV
def calculate_plot(R1, R2):
	nums = []
	for RV_resistance in range(0, RV + 1, RV//100):
		if RV_resistance == 0:
			Rparallel = 0
		else:
			Rparallel = 1 / ( 1 / RV_resistance  + 1/ R2 )
		Va = (Vin * Rparallel) / (R1 + Rparallel)
		Vout =  Rf / R2 * Va
		nums.append(Vout)
	return(nums)

# Plot voltages and normalize their range to 0-1
def calculate_normalized_plot(R1, R2):
	data = calculate_plot(R1, R2)
	gain = data[len(data) - 1]
	for i in range(len(data)):
		data[i] /= data[len(data) - 1]
	return(data)

# Compare 2 plots and return their absolute difference
def compare_plots(plot1, plot2):
	difference = 0
	for n in range(0, 101):
		difference += abs(plot1[n] - plot2[n])
	return difference

# Loop over all possible combinations of R1 and R2
return_values = []
for R1 in available_resistors:
	for R2 in available_resistors:
		plot_difference = compare_plots(calculate_normalized_plot(R1, R2), sin_plot)
		return_values.append((R1, R2, plot_difference))

# Sort plots and pick the closest ones
best_plots = sorted(return_values, key=lambda x: x[2])[:20]
for plot in best_plots:
	plt.plot(calculate_normalized_plot(plot[0], plot[1]))
	print('R1: ', plot[0] / 1000, 'k\tR2: ', plot[1] / 1000, 'k\tdifference: ', plot[2])

plt.ylabel('output voltage')
plt.xlabel('pot turn %')
plt.show()
